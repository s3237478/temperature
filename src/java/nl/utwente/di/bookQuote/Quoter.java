package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn){
        double value = (Integer.parseInt(isbn) * 9/5) + 32;
        return value;
    }
}
